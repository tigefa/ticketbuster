source 'https://rubygems.org'

ruby '2.3.1'

# Base
gem 'rails', '~> 5.0.0.1'
gem 'puma', '~> 3.4.0'
gem 'rack-canonical-host'
gem 'rack-cors'

# DB
gem 'mysql2'

# Infrastructure
gem 'bcrypt', '~> 3.1.7'
gem 'cancancan', '~> 1.10'
gem 'devise'
gem 'jbuilder', '~> 2.5'

# Monitoring
# gem 'flamegraph'
# gem 'honeybadger' # exception notification
# gem 'lograge' # goodness with logs
# gem 'rack-mini-profiler', require: false
# gem 'skylight' # monitoring performance

# Assets
gem 'bootstrap', '~> 4.0.0.alpha4'
gem 'execjs', '~> 2.7'
gem 'jquery-rails'
gem 'normalize-rails', '~> 3.0'
gem 'react-rails'
gem 'sass-rails'

# System runner
gem 'foreman'

group :development do
  gem 'better_errors'
  gem 'listen', '~> 3.0.5'
  gem 'guard', '2.13.0'
  gem 'guard-spring'
  gem 'guard-rspec', '~> 4.7.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console'
end

group :development, :test do
  gem 'awesome_print'
  gem 'bullet' # help kill n+1
  gem 'bundler-audit', '>= 0.5', require: false
  gem 'factory_girl_rails', '~> 4.0'
  gem 'pry-rails'
  gem 'rspec'
  gem 'rspec-rails'
end

group :test do
  gem 'codeclimate-test-reporter', require: false
  gem 'database_cleaner', require: false
  gem 'shoulda-matchers', '~> 3.1'
  gem 'webmock'
end

group :staging, :production do
  gem 'rack-timeout'
  gem 'rails_stdout_logging'
  gem 'rails_12factor'
end

gem 'dotenv-rails'
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
