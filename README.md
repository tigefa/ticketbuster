# TicketBuster 
This is the official repository for TicketBuster, a basic customer service support request ticketing system.
    
Note, this is a living draft and subject to change. Check the commit history! 

## Demo and Usage
Click [here](https://ticketbuster.herokuapp.com) for a live demo. Login with user `admin` and password `powercorrupts`

## Local Installation
Refer to the docker website (https://docs.docker.com/engine/installation/) to ensure you have both docker and docker-compose installed on your development machine. Everything else is built within the container. 

To get started: 

* Clone the latest version of this repository with `git clone https://gitlab.com/bitworks/ticketbuster.git`

* `cd ticketbuster`

* `mv config/env/development .env && mv config/env/db .env.db`

* `docker-compose up`

The first time you run `docker-compose up` will take quite some time, as the images will need to be pulled and built locally. Successive runs of the container will be much faster. 

### Spec suite
Run specs or other commands as such:

#### rspec -- full suite

* `docker-compose exec api rspec`

The `ENTRYPOINT` definition in `Dockerfile` will be pre-pended to all commands run with `docker-compose exec`
    
## Implementation Details

TicketBuster is built on Rails 5.0.0.1 API mode, utilizing [React](https://facebook.github.io/react/docs/getting-started.html) via [react-rails](https://github.com/reactjs/react-rails) for front end component rendering. 

Authorization and tenancy is handled with [Devise](https://github.com/platformatec/devise) and CanCanCan.

Puma handles requests and mysql2 interfaces with MySQL via networked Docker containers, to maintain a service-based composable architechture. 

## API Documentation

See `DESIGNDOC.md` for detailed API documentation.