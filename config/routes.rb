Rails.application.routes.draw do

  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      resources :tickets do
        member do
          post :new
        end
        collection do
          post :new
          get :open
        end
      end
    end
  end
end
