FactoryGirl.define do
  factory :ticket do
    summary "This content summarizes the issue"
    content "This content describes in detail the situation that is present."
    status 1
    priority 1
    created_at Time.now
    updated_at 10.minutes.from_now

    # transient do
    #   comments_count 5
    # end

    # after(:build) do |ticket, evaluator|
    #   create_list(:comment, evaluator.comments_count, ticket: ticket)
    # end
  end
end
