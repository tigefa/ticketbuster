require 'rails_helper'

RSpec.describe Ticket do
  it 'has a valid factory' do
    expect(build(:ticket)).to be_valid
  end

  let(:ticket) { build(:ticket) }

  it 'is valid with valid attributes' do
    expect(ticket).to be_valid
  end
end

RSpec.describe Ticket, 'ActiveModel validations' do
  it { should validate_presence_of(:summary) }
  it { should validate_presence_of(:priority) }
  it { should validate_presence_of(:created_at) }
  it { should validate_presence_of(:updated_at) }
end

RSpec.describe Ticket, '#' do
  true
end