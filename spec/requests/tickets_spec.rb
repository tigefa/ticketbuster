require 'rails_helper'

# api_v1_ticket_path
RSpec.describe 'POST /api/v1/tickets/:id' do
  it 'returns OK with valid JSON body' do
    ticket = create(:ticket)
    params = attributes_for(:ticket)

    post "/api/v1/tickets/#{ticket.id}", ticket: params
    expect(response.status).to eq(204)
    expect(Ticket.last.summary).to eq params[:summary]
  end
end

# api_v1_tickets_path
RSpec.describe 'POST /api/v1/tickets' do
  it 'returns OK with valid JSON body' do
    params = attributes_for(:ticket)

    post '/api/v1/tickets', ticket: params

    expect(response.status).to eq(204)
    expect(Ticket.first.summary).to eq params[:summary]
  end
end

RSpec.describe 'GET /api/v1/tickets' do
  it 'returns a list of all tickets, highest (1) priority first' do
    create(:ticket, priority: 3)
    create(:ticket, priority: 1)

    get '/api/v1/tickets'

    high_priority_json = json_body["tickets"][0]
    expect(json_body["tickets"].count).to eq(Ticket.all.count)
    expect(high_priority_json["priority"]).to eq(1)
  end
end

RSpec.describe 'GET /api/v1/tickets/:id' do
  it 'returns the specific ticket body' do
    ticket = create(:ticket)

    get "/api/v1/tickets/#{ticket.id}"

    expect(response.status).to eq(200)
    expect(json_body["ticket"]["content"]).to eq(ticket.content)
  end
end

RSpec.describe 'DELETE /api/v1/tickets/:id', type: :request do
  it 'deletes the specific ticket record' do

    ticket = create(:ticket)

    expect(Ticket.all.count).to eq(1)

    delete "/api/v1/tickets/#{ticket.id}"

    expect(response.status).to eq(200)
    expect(Ticket.all.count).to eq(0)
  end
end
