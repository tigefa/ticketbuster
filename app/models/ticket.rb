class Ticket < ApplicationRecord
  validates_presence_of(:summary, :created_at, :updated_at, :priority)
  belongs_to :customer
  has_one :agent
  has_many :comments
end
 
