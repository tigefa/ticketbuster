class Api::V1::TicketsController < Api::V1::ApplicationController
  def new
    @ticket = Ticket.create(ticket_params)
  end

  def create
    @ticket = Ticket.create(ticket_params)
    if @ticket.save!
      render json: { ticket: "word" }, status: 201
    else
      render json: { ticket: @ticket.errors.full_messages.as_json }, status: 400
    end
  end

  def update
  end

  def destroy
    @ticket = Ticket.find_by(id: params['id'])
    if @ticket.delete
      render json: { status: :ok }
    else
      render json: { response: @ticket.errors.full_messages.as_json }
    end
  end

  def show
    @ticket = Ticket.find_by(id: params['id'])
    render json: { ticket: @ticket }
  end

  def index
    render json: { tickets: Ticket.order('priority') }
  end

  private

  def ticket_params
    params.require(:ticket).permit(:summary, :content, :status, :priority, :created_at, :updated_at)
  end
end
