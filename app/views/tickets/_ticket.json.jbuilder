json.extract! ticket, :id, :summary, :status, :priority, :content, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)